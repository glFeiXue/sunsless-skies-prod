**Before anything else, big shoutout to [1410c](https://steamcommunity.com/id/1410c) for excellent [guide](https://steamcommunity.com/sharedfiles/filedetails/?id=1456294858) on SS save file editing and specially for compiling impressive list of game data**

   Script configuration is in *config.txt* which has to exist and have formatting like provided one. All keys that are in provided file must exist! Have a look, it won't bite.

   Script will attempt to figure out where your save file is based on OS it detects. Currently it is aware of how to proceed on Linux and Windows. Will add support for Mac when somebody kindly tells me where game save is there. Google apparently doesn't know.

   If it fails then first look at *platform.dbg* file in OUTPUT_DIR directory for clues. Or modify SAVE_DIR key in *config.txt* to point to right directory.
   
   OUTPUT_DIR and BACKUP_DIR are relative to current working directory. LINUX_DIR and WIN_DIR are relative to your home directory; /home/*username* on Linux and SYSTEM_DISK:/Users/*username* on Windows. If SAVE_DIR is anything but [default] it is considered absolute path.

   ------------------------------------------------------------
   List or commands, their arguments and what they (should) do:
   ------------------------------------------------------------
  
 **`-l`**

    list values for IDs in 'QUICK_IDS'
  
   There are two files with IDs used in save file. Unless you change defines in script they are *quick_ids.txt* and *known_ids.txt*
     
   Of course the former is just a subset of the latter. They are kept separate because usually you regulary check just few 'everyday' values. But feel free to expand *quick_ids.txt* with lines from *known_ids.txt*, just make sure you copy properly. Should you discover unlisted ID or find name for existing ID, adding lines is easy. They are just 'ID = name' pairs.
   
   
 **`-cmp`**

    compare current inventory with one previously saved to 'inventory.txt'
  
   Both `-l` and `-p` commands will save current inventory to 'inventory.txt'. This is mostly useful in hunt for unknown IDs. You'd save the game, run either `-l` or `-p`, play a bit while noting changes, save again and run `-dif`. 
   
   
 **`-b`**

    create backup of current game save in 'BACKUP_DIR'

   BACKUP_DIR **must** exist!
  
   Backup file names are in form of *autosave_s.json.###*. Each invocation of `-b` increases '###' by one. You can delete old backups in which case count will start at lowest empty slot.

   For example; you have made 27 backups and decide to delete first 15. Next backup will have number 001. They'll increase to 015 and then jump to 028.


**`-p <string>`**

    show value(s) for items with names containing 'string'
    note: no wildcards
         
   Sometimes you want to see a value saved that isn't in your 'quick' list. So you'd for instance do:
   
    `php ss.php -p "Wit & Vinegar 'Zounderkite'"`
    or just:
    `php ss.php -p wit`
    
   Obviously, if what you search for doesn't exist in 'ALL_IDS' script won't be able to fetch ID and will thus fail.
   
   
**`-w`**

    display world info
  
   Well, save to file to be precise. Basic world info with region IDs, region segments and fog info. read it, should be pretty self explanatory.
   
   
**`-df <regionID>`**

    defog region
    use -w to get region IDs
  
   There are 10,000 fog tiles for each region. Yep, 10 thousands. On your discoveries you 'pop' them one by one. Or you could just run this command and be done with it. Note that this will NOT reveal landmarks nor set them as discovered. So it is best used when you've explored the map somewhat and are just annoyed by those black patches. Or maybe it isn't.
   

**`-c <name> <value>`**

    change value for variable 'name' to 'value'
    can be used multiple times
    variable name can be partial
  
   Why the hell haven't I put this at the top and save you a bunch of reading, right? So, any changes to save file will be on a copy of said file. In fact, script only operates on copy of save filer, never save file itself. Example of usage would be something like
     `php ss.php -c gol 10000 -c hul 50`
   which would create two files. First one with changed Gold value, set to 10,000 and second with additional Hull change to 50. You can list as many -c <name> <value> triples as you want. If script encounters a name it can't find in *quick_ids.txt* it will end but all changes up to that point will be still done. For the super lazy; first match will be used when searching for IDs matching 'name'.

   If item doesn't exist in your inventory it will be added. Again, if what you search for doesn't exist in 'ALL_IDS' script won't be able to fetch ID and will thus fail.
	   
   Note: if you used -hm command then all subsequent modifications of Hull (either with -c or -hm) will fail. Working on it.
    
**`-hm <base> <modifier>`**

    set Hull to 'base' and its modifier to 'modifier'
    effectively giving you 'base'+'modifier' Hull
  
   Hull gets special command which in my experience renders your locomotive indestructable. Maybe I'll expand this command to other properties that could benefit but they need to be tested. I've been flying around with 0 Hull for days now since if there's an event that lowers Modifier I'm yet to experience it.
   
   
**`-s <valKey> <value>`**

    search inventory for specific 'value'
    of one of 3 possible property keys
     EL = EffectiveLevel
    ELM = EffectiveLevelModifier
      L = Level

   For the explorers out there; this will go through save file and find all occurences of 'value' in one of three possible places within each entry. Property keys are case-insensitive. So if you are on that hunt for Nightmares, you'd do
    `php ss.php -s el 5` or whatever your Nightmares are at and then change values one by one with hope of finding which of the x IDs is the correct one.
   

**`-tp <port name>`**

	teleport to 'port name'
   

**`-v`**

	display bank contents
	
   =====================================================================================================================

   All of these commands will, additionaly to print-out, produce various files. They will tell you about it. Not extentionaly but should suffice. If DEBUG=1 in config.txt, script will write some additional files into 'output'; all these have extension .dbg. All files produced are plain text files. When displaying values script only knows how to handle the usuall suspects, EffectiveLevel, EffectiveLevelModifier and Level. So for items that use different property keys (like weapons, scouts, etc) it'll display no values, just let you know if item is in your inventory.
  
