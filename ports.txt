;  The Reach
CommonName:New Winchester,CurrentRegionId:9f50ee22-a594-4c42-939c-022cbe2fb677,CurrentPortId:8576ef12-9e4d-4f30-8116-db5eb80f9543,LocationName:New Winchester,CurrentSegmentPosition:{}
CommonName:Hybras,CurrentRegionId:9f50ee22-a594-4c42-939c-022cbe2fb677,CurrentPortId:86641b94-c0de-48d5-b1c5-7fcdd70d3882,LocationName:Hybras,CurrentSegmentPosition:{"Ring":1}
CommonName:Polmear and Plenty's Inconceivable Circus,CurrentRegionId:9f50ee22-a594-4c42-939c-022cbe2fb677,CurrentPortId:8fcc687f-1e9b-444a-a758-c402d0608733,LocationName:The Inconceivable Circus,CurrentSegmentPosition:{"Ring": 1/"Index": 3}
CommonName:Port Avon,CurrentRegionId:9f50ee22-a594-4c42-939c-022cbe2fb677,CurrentPortId:89aff93f-4b2e-4e03-bb41-d6054a84c5ee,LocationName:Port Avon,CurrentSegmentPosition:{"Ring":1/"Index":1}
CommonName:Traitor's Wood,CurrentRegionId:9f50ee22-a594-4c42-939c-022cbe2fb677,CurrentPortId:660ff730-50ea-4190-8210-a232c6e7b087,LocationName:Traitor's Wood,CurrentSegmentPosition:{"Ring":1/"Index":1}
CommonName:Leadbeater & Stainrod's Nature Reserve,CurrentRegionId:9f50ee22-a594-4c42-939c-022cbe2fb677,CurrentPortId:a3e0d4ac-b595-42b6-8b10-349478fc7874,LocationName:Leadbeater & Stainrod Reserve,CurrentSegmentPosition:{"Ring":1}
CommonName:Titania,CurrentRegionId:9f50ee22-a594-4c42-939c-022cbe2fb677,CurrentPortId:7f7247f1-34b4-4f57-95d8-d482105edcb2,LocationName:Titania,CurrentSegmentPosition:{"Ring":1}
CommonName:Magdalene's,CurrentRegionId:9f50ee22-a594-4c42-939c-022cbe2fb677,CurrentPortId:21004c53-ad6e-43e4-8df4-0230da3e87e6,LocationName:Magdalene's,CurrentSegmentPosition:{"Ring":1/"Index":4}
CommonName:Port Prosper,CurrentRegionId:9f50ee22-a594-4c42-939c-022cbe2fb677,CurrentPortId:6c1c3a55-b424-4bec-a969-31023ace58e7,LocationName:Prosper,CurrentSegmentPosition:{"Ring":1/"Index":4}
CommonName:Carillon,CurrentRegionId:9f50ee22-a594-4c42-939c-022cbe2fb677,CurrentPortId:f3ca7b9d-a6ba-4d2a-bbd1-b12d4ec25036,LocationName:Carillon,CurrentSegmentPosition:{"Ring":1/"Index":2}
CommonName:Lustrum,CurrentRegionId:9f50ee22-a594-4c42-939c-022cbe2fb677,CurrentPortId:c22807a9-7c42-41b5-823a-cc02f15c3f63,LocationName:Lustrum,CurrentSegmentPosition:{"Ring":1/"Index":2}
;  Albion
CommonName:London,CurrentRegionId:3ea179aa-2a9c-47c5-9dc3-20fee32fc228,CurrentPortId:fc40ea42-ec67-4e47-9ec0-8a8c1b41af7a,LocationName:London,CurrentSegmentPosition:{}
CommonName:The Ministries,CurrentRegionId:3ea179aa-2a9c-47c5-9dc3-20fee32fc228,CurrentPortId:289aa8ac-754d-4610-9639-cc888c3f6eac,LocationName:The Ministries,CurrentSegmentPosition:{}
CommonName:Floating Parliament,CurrentRegionId:3ea179aa-2a9c-47c5-9dc3-20fee32fc228,CurrentPortId:b301a570-2040-44e9-b679-aca62bac89f7,LocationName:Floating Parliament,CurrentSegmentPosition:{"Ring":1}
CommonName:The Brabazon Workworld,CurrentRegionId:3ea179aa-2a9c-47c5-9dc3-20fee32fc228,CurrentPortId:86643f84-680c-415a-bf00-d4dceb891189,LocationName:Brabazon Workworld,CurrentSegmentPosition:{"Ring":1}
CommonName:The Royal Society,CurrentRegionId:3ea179aa-2a9c-47c5-9dc3-20fee32fc228,CurrentPortId:731c7b0d-d06d-4681-a3b3-d265eaea8d82,LocationName:Royal Society,CurrentSegmentPosition:{"Ring":1/"Index":1}
CommonName:The Clockwork Sun,CurrentRegionId:3ea179aa-2a9c-47c5-9dc3-20fee32fc228,CurrentPortId:bfc72a01-6af5-4c74-9354-74848b97c35d,LocationName:Clockwork Sun,CurrentSegmentPosition:{"Ring":1/"Index":1}
CommonName:Perdurance,CurrentRegionId:3ea179aa-2a9c-47c5-9dc3-20fee32fc228,CurrentPortId:dd23161a-eafa-4b19-be78-bb27d5c340d2,LocationName:Perdurance,CurrentSegmentPosition:{"Ring":1/"Index":1}
CommonName:Avid Horizon,CurrentRegionId:3ea179aa-2a9c-47c5-9dc3-20fee32fc228,CurrentPortId:1b331350-d950-4c06-94e0-38dd736859d7,LocationName:Avid Horizon,CurrentSegmentPosition:{"Ring":1/"Index":3}
CommonName:Mausoleum,CurrentRegionId:3ea179aa-2a9c-47c5-9dc3-20fee32fc228,CurrentPortId:358df020-9a27-47c3-8a80-6f1c836898df,LocationName:Mausoleum,CurrentSegmentPosition:{"Ring":1/"Index":2}
CommonName:Worlebury-juxta-Mare,CurrentRegionId:3ea179aa-2a9c-47c5-9dc3-20fee32fc228,CurrentPortId:359b836b-864d-4587-8527-e2d2c360fdeb,LocationName:Worlebury-juxta-Mare,CurrentSegmentPosition:{"Ring":1/"Index":2}
CommonName:Wit and Vinegar Lumber,CurrentRegionId:3ea179aa-2a9c-47c5-9dc3-20fee32fc228,CurrentPortId:495ca56d-e543-417b-a431-8e3ce0a4f8cc,LocationName:Wit & Vinegar Lumber Co.,CurrentSegmentPosition:{}
CommonName:The Stair to the Sea,CurrentRegionId:3ea179aa-2a9c-47c5-9dc3-20fee32fc228,CurrentPortId:da9669de-b852-4587-b68d-a8830e555840,LocationName:The Stair to the Sea,CurrentSegmentPosition:{"Ring":1/"Index":2}
;  Eleutheria
CommonName:Pan,CurrentRegionId:7e3ccf00-f0a8-47c1-b444-33e2f84a82b1,CurrentPortId:bb49fdab-1ebe-45d9-97cb-9ab39db8d2a8,LocationName:Pan,CurrentSegmentPosition:{}
CommonName:Piranesi,CurrentRegionId:7e3ccf00-f0a8-47c1-b444-33e2f84a82b1,CurrentPortId:06401842-00d7-463c-8d6b-6fdd41b2edae,LocationName:Piranesi,CurrentSegmentPosition:{"Ring":1}
CommonName:Eagle Empyrean,CurrentRegionId:7e3ccf00-f0a8-47c1-b444-33e2f84a82b1,CurrentPortId:dad2e4cc-5983-41db-8e20-d07104e92666,LocationName:Eagle's Empyrean,CurrentSegmentPosition:{"Ring":1}
CommonName:House of Rods and Chains,CurrentRegionId:7e3ccf00-f0a8-47c1-b444-33e2f84a82b1,CurrentPortId:b14c0b6b-839c-4311-bfd5-518edeea28ad,LocationName:House of Rods and Chains,CurrentSegmentPosition:{"Ring":1/"Index":2}
CommonName:Langley Hall,CurrentRegionId:7e3ccf00-f0a8-47c1-b444-33e2f84a82b1,CurrentPortId:418df4ef-902a-4ae9-9673-31e2978ce7b7,LocationName:Langley Hall,CurrentSegmentPosition:{"Ring":1/"Index":1}
CommonName:Caduceus,CurrentRegionId:7e3ccf00-f0a8-47c1-b444-33e2f84a82b1,CurrentPortId:44773c30-e3dd-4ce4-a8bd-fec7b55bb463,LocationName:Caduceus,CurrentSegmentPosition:{"Ring":1/"Index":1}
CommonName:Achlys,CurrentRegionId:7e3ccf00-f0a8-47c1-b444-33e2f84a82b1,CurrentPortId:9e91ea8e-3f0f-4a25-a400-2f451d91da81,LocationName:Achlys,CurrentSegmentPosition:{"Ring":1}
CommonName:Winter's Reside,CurrentRegionId:7e3ccf00-f0a8-47c1-b444-33e2f84a82b1,CurrentPortId:83901830-f638-46e2-a130-b198d0552c84,LocationName:Winter's Reside,CurrentSegmentPosition:{}
;  *** duplicate port ID - Wit and Vinegar Lumber Company
;  ***CommonName:The Brazen Brigade,CurrentRegionId:7e3ccf00-f0a8-47c1-b444-33e2f84a82b1,CurrentPortId:495ca56d-e543-417b-a431-8e3ce0a4f8cc,LocationName:The Brazen Brigade,CurrentSegmentPosition:{}
CommonName:The Gentlemen,CurrentRegionId:7e3ccf00-f0a8-47c1-b444-33e2f84a82b1,CurrentPortId:72c7faeb-59a4-4726-9ebd-2b76b94c749b,LocationName:The Gentlemen,CurrentSegmentPosition:{}
CommonName:Heart-Catcher Gardens,CurrentRegionId:7e3ccf00-f0a8-47c1-b444-33e2f84a82b1,CurrentPortId:3b960cb2-0335-4e16-935b-0e8000c0628a,LocationName:Heart-Catcher Gardens,CurrentSegmentPosition:{}
;  The Blue Kingdom
CommonName:The Forge of Souls,CurrentRegionId:25d750b5-bb46-409c-8a94-5676bad0df17,CurrentPortId:8a6a2b39-691a-4373-a2d6-d0ef8f9853c7,LocationName:The Forge of Souls,CurrentSegmentPosition:{}
CommonName:Sky Barnet,CurrentRegionId:25d750b5-bb46-409c-8a94-5676bad0df17,CurrentPortId:f6d9ca88-cece-47ea-a973-264a37df87d7,LocationName:Sky Barnet,CurrentSegmentPosition:{}
CommonName:House of Days,CurrentRegionId:25d750b5-bb46-409c-8a94-5676bad0df17,CurrentPortId:c74be88b-b3dc-48e8-8659-ace9a2ca719a,LocationName:House of Days,CurrentSegmentPosition:{}
CommonName:The Shadow of the Sun,CurrentRegionId:25d750b5-bb46-409c-8a94-5676bad0df17,CurrentPortId:692cba21-55a3-4b38-b493-ba8849d0d123,LocationName:The Shadow of the Sun,CurrentSegmentPosition:{}
CommonName:Death's Door,CurrentRegionId:25d750b5-bb46-409c-8a94-5676bad0df17,CurrentPortId:97191169-a980-43eb-b965-d23c80cb0f85,LocationName:Death's Door,CurrentSegmentPosition:{}
CommonName:Wellmouth,CurrentRegionId:25d750b5-bb46-409c-8a94-5676bad0df17,CurrentPortId:fbbe2385-9207-4bbb-870c-358bbd755341,LocationName:Wellmouth,CurrentSegmentPosition:{}
CommonName:The Stone Faced Court,CurrentRegionId:25d750b5-bb46-409c-8a94-5676bad0df17,CurrentPortId:27259fac-64b1-4385-ae4c-f84235504dee,LocationName:The White Well,CurrentSegmentPosition:{}
