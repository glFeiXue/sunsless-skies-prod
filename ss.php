<?php
	$cfgA=innit();
	if(!$cfgA[0]) {
		println("ERR:");
		println($cfgA[1],1);
		exit(1);
	}	
	$config=$cfgA[1];
				
	if($argc<2) {
		usage();		
		exit(1);
	}

	if(array_search("-b",$argv)!==false) {
		$bA=backup();
		if(!$bA[0]) {
			println("ERR:");
			println($bA[1],1);
			exit(1);
		} else {
			println("Backup created: ".$bA[1],1);
			exit(0);		
		}
	}
		
	$idsA=readIDs();
	if(!$idsA[0]) {
		println("ERR:");
		println($idsA[1],1);
		exit(1);	
	}
	$ids=$idsA[1];
	
	$allIDsA=readAllIDs();
	if(!$allIDsA[0]) {
		println("NOTE:");
		println($allIDsA[1],1);
		$allIDs=false;
	} else {
		$allIDs=$allIDsA[1];
	}	
	
	$tpc=false;
	$tpA=readPorts();
	if(!$tpA[0]) {
		println("NOTE:");
		println("Failed to read 'ports.txt'.",1);
		println($tpA[1],2);
		println("You will not be able to use -tp command.\n",1);	
	} else {
		$ports=$tpA[1];
		println("Read ".count($ports)." ports",1);
		$tpc=true;
	}
	
	$savedA=readSaveFile();
	if(!$savedA[0]) {
		println("ERR:");
		println($savedA[1],1);
		exit(1);	
	}	
	$fileData=$savedA[1];
		
	println("Read ".number_format(strlen($fileData)/1000,1)." kB,   Debug is ".($config["DEBUG"]?"ON":"OFF"),1);
		
	$adA=arrFromJson($fileData);
	if(!$adA[0]) {
		println("ERR:");
		println($adA[1],1);
		exit(1);			
	}
	$arrData=$adA[1];

	if(array_search("-cmp",$argv)!==false) {
		$bA=readSavedInv($arrData);
		if(!$bA[0]) {
			println("ERR:");
			println($bA[1],1);
			exit(1);
		} else {
			println("Diff file written",1);
			exit(0);		
		}
	}
	
	$wdA=worldData($arrData,true);
	if(!$wdA[0]) {
		println("ERR:");
		println($wdA[1],1);
		exit(1);
	} else {
		if($tpc) {
			if(array_key_exists($wdA[1][0],$ports)) {
				println("Current location: ".$ports[$wdA[1][0]]["CommonName"],1);
			} else {
				println("NOTE:",1);
				println("Unknown port ID: ".$wdA[1][0],2);
				println(" save file says: ".$wdA[1][1],2);
		
				if($config["DEBUG"]) {
					$dbgA=newPort($wdA[1]);
					if(!$dbgA[0]) {
						println("ERR:");
						println($dbgdA[1],1);
						exit(1);		
					}	else {
						println("see 'newport.dbg' for more info",2);				
					}
				}				
			}
		} else {
			println("Current location: `".$wdA[1][1]."`",1);		
		}	
	}
			
	if(array_search("-w",$argv)!==false) {
		$wdA=worldData($arrData);
		if(!$wdA[0]) {
			println("ERR:");
			println($wdA[1],1);
			exit(1);
		} else {
			exit(0);		
		}
	}
			
	if(array_search("-v",$argv)!==false) {
		$wdA=bankContent($arrData);
		if(!$wdA[0]) {
			println("ERR:");
			println($wdA[1],1);
			exit(1);
		} else {
			displayValues($wdA[1],true,60,true,"Bank Contents");
			exit(0);		
		}
	}
	
	$dfP=array_search("-df",$argv);
	if($dfP!==false) {
		if(!isset($argv[$dfP+1])) {
			println("ERR:");
			println("Command -df requires region ID",1);
			exit(1);		
		}
		$regId=$argv[$dfP+1];
		$dfA=defogRegion($fileData,$regId);
		if(!$dfA[0]) {
			println("ERR:");
			println($dfA[1],1);
			exit(1);	
		}
		exit(0);
	}
		
	$QPLStart=strrpos($fileData,"QualitiesPossessedList");
	if($QPLStart===false) {
		println("ERR:");
		println("'QualitiesPossessedList' key does not exist",1);
		exit(1);
	}

	$findKey=false;
	$findVal=false;
	$display=true;
	$dfP=array_search("-s",$argv);
	if($dfP!==false) {
		if(!isset($argv[$dfP+1]) || !isset($argv[$dfP+2])) {
			println("ERR:");
			println("Command -s requires property key and a number",1);
			exit(1);		
		}
		if(!is_numeric($argv[$dfP+2])) {
			println("ERR:");
			println("Command -s requires a number as second parameter",1);
			exit(1);		
		}
		$findKey=$argv[$dfP+1];
		$findVal=$argv[$dfP+2];
		$argv[$dfP]="-l";
		$display=false;
	}

	if(array_search("-l",$argv)!==false) {			
		$valA=getValues($ids,$arrData,true,$findKey,$findVal);
		if(!$valA[0]) {
			println("ERR:");
			println($valA[1],1);
			exit(1);	
		}	
		$vals=$valA[1];

		if($display) {
			displayValues($vals);
		}
			
		exit(0);
	}

	$dfP=array_search("-p",$argv);
	if($dfP!==false) {
		if(!isset($argv[$dfP+1])) {
			println("ERR:");
			println("Command -p requires string to search for",1);
			exit(1);		
		}
		$pat=$argv[$dfP+1];
		$ids=findCandidates($pat);
		
		if(!$ids) {
			println("NOTE:");
			println("No known items matching ".$pat,1);
			exit(0);		
		}
		
		$valA=getValues($ids,$arrData,true);
		if(!$valA[0]) {
			println("ERR:");
			println($valA[1],1);
			exit(1);	
		}	
		$vals=$valA[1];

		if(!$vals) {
			println("NOTE:");
			println("No item matching ".$pat." found in inventory",1);
			exit(0);	
		}
		displayValues($vals);
		exit(0);
	}
		
	$dfP=array_search("-hm",$argv);
	if($dfP!==false) {
		if(!isset($argv[$dfP+1]) || !isset($argv[$dfP+2])) {
			println("ERR:");
			println("Command -hm requires two numbers",1);
			exit(1);		
		}
		if(!is_numeric($argv[$dfP+1]) || !is_numeric($argv[$dfP+2])) {
			println("ERR:");
			println("Command -hm requires two numbers",1);
			exit(1);		
		}
		$hbase=$argv[$dfP+1];
		$hmod=$argv[$dfP+2];
		$mhA=modHull($hbase,$hmod,$allIDs);
		if(!$mhA[0]) {
			println("ERR:");
			println($mhA[1],1);
			exit(1);
		} else {
			$wrA=writeResult($fileData,"autosave_s.json.mod.v1","Saved to autosave_s.json.mod.v1",1);
			if(!$wrA[0]) {
				println("ERR:");
				println($wrA[1],1);
				exit(1);
			}
			exit(0);
		}
	}
	
	$dfP=array_search("-tp",$argv);
	if($dfP!==false) {
		if(!$tpc) {
			println("ERR:");
			println("Command -tp requires valid 'ports.txt'",1);
			exit(1);					
		}
		if(!isset($argv[$dfP+1])) {
			println("ERR:");
			println("Command -tp requires port name",1);
			exit(1);		
		}
		
		$pIDA=findPort($argv[$dfP+1],$ports);
		if(!$pIDA[0]) {
			println("ERR:");
			println($pIDA[1],1);
			exit(1);
		}
		$dport=$pIDA[1];
		
		$tpA=teleport($dport);
		if(!$tpA[0]) {
			println("ERR:");
			println($tpA[1],1);
			exit(1);
		} else {
			$wrA=writeResult($fileData,"autosave_s.json.tp","Saved to autosave_s.json.tp",1);
			if(!$wrA[0]) {
				println("ERR:");
				println($wrA[1],1);
				exit(1);
			}
			println("Teleported to ".$dport["CommonName"],1);
			exit(0);
		}	
	}
		
	if(array_search("-c",$argv)!==false) {
		$valA=getValues($ids,$arrData);
		if(!$valA[0]) {
			println("ERR:");
			println($valA[1],1);
			exit(1);	
		}	
		$vals=$valA[1];
					
		$caA=fillChangeArray($argc,$argv);
		if(!$caA[0]) {
			println("ERR:");
			println($caA[1],1);
			exit(1);	
		}
		$changeArr=$caA[1];
		
		$ver=1;
		$dbg="";
		
		println("Changing",1);
		foreach($changeArr as $id=>$v) {
			$lbl=$allIDs[$id];
			if(strlen($lbl)>16) {
				$lbl=substr($lbl,0,13)."...";			
			}
			
			if(isset($vals[$id]["EffectiveLevel"])) {
				echo "    ".sprintf("%6d: %-16s from %6d/%6d to %6d",$id,$lbl,$vals[$id]["Level"],$vals[$id]["EffectiveLevel"],$v)." ... ";
				$chDataA=changeValue($id,$v);
			} else {
				echo "    ".sprintf("%6d: %-16s add  %6d",$id,$lbl,$v)."                  ... ";
				$chDataA=insertValue($id,$v);
			}		
			
			if(!$chDataA[0]) {
				println("ERR");
				println($chDataA[1],1);
				
				if($config["DEBUG"]) {
					$wrA=writeResult($dbg,"ss.log");
					if(!$wrA[0]) {
						println("ERR:");
						println($wrA[1],1);
						exit(1);
					}
				}
						
				exit(1);		
			}
			
			echo "OK\n";
			
			$wrA=writeResult($fileData,"/autosave_s.json.mod.v".$ver,"Saved to autosave_s.json.mod.v".$ver,3);
			if(!$wrA[0]) {
				println("ERR:");
				println($wrA[1],1);
				exit(1);
			}
			
			$ver++;
		}
			
		if($config["DEBUG"]) {
			$wrA=writeResult($dbg,"ss.log");
			if(!$wrA[0]) {
				println("ERR:");
				println($wrA[1],1);
				exit(1);
			}
		}
		
		exit(0);
	}
	
// ===================================================

function readIDs() {
	global $config;
	
	$cwd=getcwd();
	$ffname=$cwd."/".$config["QUICK_IDS"];
	
	if(!file_exists($ffname))
		return array(false,"File ".$config["QUICK_IDS"]." does not exist");
		
	$fc=file_get_contents($ffname);
	if(!$fc)
		return array(false,"Could not read ".$config["QUICK_IDS"]);
		
	$fLA=explode(PHP_EOL,$fc);
	if(!$fLA)
		return array(false,"Unrecognizable file format: ".$config["QUICK_IDS"]);
		
	$idsA=array();
	$ln=1;
	foreach($fLA as $l) {
		if(!$l) {
			$ln++;
			continue;
		}
		
		$lA=explode("=",$l);
		if(count($lA)!=2) {
			//echo print_r($fLA,true)."\n";
			return array(false,"Wrong line format in ".$config["QUICK_IDS"]." at ".$ln);		
		}
		
		$idsA[trim($lA[0])]=trim($lA[1]);
		$ln++;
	}
	
	return array(true,$idsA);
}	

// -----------------------------------------------------

function readAllIDs() {
	global $config;

	$cwd=getcwd();
	$ffname=$cwd."/".$config["ALL_IDS"];
	
	if(!file_exists($ffname))
		return array(false,"File ".$config["ALL_IDS"]." does not exist");
		
	$fc=file_get_contents($ffname);
	if(!$fc)
		return array(false,"Could not read ".$config["ALL_IDS"]);
		
	$fLA=explode(PHP_EOL,$fc);
	if(!$fLA)
		return array(false,"Unrecognizable file format: ".$config["ALL_IDS"]);
		
	$idsA=array();
	$ln=1;
	foreach($fLA as $l) {
		if(!$l) {
			$ln++;
			continue;
		}
		
		$lA=explode("=",$l);
		if(count($lA)!=2) {
			//echo print_r($fLA,true)."\n";
			return array(false,"Wrong line format in ".$config["ALL_IDS"]." at ".$ln);		
		}
		
		$idsA[trim($lA[0])]=trim($lA[1]);
		$ln++;
	}
	
	return array(true,$idsA);
}	

// -----------------------------------------------------

function readSaveFile() {
	global $config;

	$sfA=resolveDir();
	if(!$sfA[0])
		return array(false,$sfA[1]);
	
	$sfname=$sfA[1]."/autosave_s.json";
	if(!file_exists($sfname)) 
		return array(false,"File\n  ".$sfname."\n  does not exist");
	
	$dfname=getcwd()."/".$config["OUTPUT_DIR"]."/autosave_s.json";
	$wr=copy($sfname,$dfname);
	if(!$wr)
		return array(false,"Failed to copy save file to ".$config["OUTPUT_DIR"]);	
	
	$fc=file_get_contents($dfname);
	
	return array(true,$fc);
}

// -----------------------------------------------------

function arrFromJson($fileData) {
	global $config;
	
	$json=json_decode($fileData,true);

	if($config["DEBUG"]) {
		$wrA=writeResult(print_r($json,true),"fullSave.dbg");
		if(!$wrA[0]) {
			return array(false,$wrA[1]);		
		}			
	}
	
	return array(true,$json);
}

// -----------------------------------------------------

function getValues($ids,$saved,$quick=false,$findKey=false,$findVal=false) {
	global $config, $allIDs;
	
	if(!isset($saved["QualitiesPossessedList"]))
		return array(false,"'QualitiesPossessedList' key does not exist");
	
	if($findKey)
		$findKey=strtoupper($findKey);
	
	$findRes=array();
			
	$inv=$saved["QualitiesPossessedList"];

	if($config["DEBUG"]) {
		$wrA=writeResult(print_r($inv,true),"inventory.dbg");
		if(!$wrA[0]) {
			return array(false,$wrA[1]);		
		}
	}
	
	$blankItem=array(	"Name"						=> false,
							"EffectiveLevel"			=> false,
							"EffectiveLevelModifier"=> false,
							"Level"						=> false,
							"Tag"							=> false,
							"ArrayIdx"					=> false,
							"CommonName"				=> "  unknown"
						);

	$countA=array(	"ItemsAll"	=> count($inv),
						"ItemsId"	=> 0,
						"ItemsOwn"	=> 0
				);
				
	$values=array();
	$result=" ID      EL       ELM      L        Name        Tag         Common name\n";
	
	foreach($inv as $idx=>$item) {
		if(isset($item["AssociatedQuality"]["Id"])) {
			$countA["ItemsId"]++;
			$id=$item["AssociatedQuality"]["Id"];
			$values[$id]=$blankItem;
			$values[$id]["ArrayIdx"]=$idx;
			$values[$id]["EffectiveLevel"]=0;
			$values[$id]["Level"]=0;
			
			$result.=sprintf("%6d: ",$id);
			$commonName="  unknown";

			if(array_key_exists($id,$allIDs)) {
				$commonName=$allIDs[$id];
			}
			
			$values[$id]["CommonName"]=$commonName;

			if(isset($item["EffectiveLevel"])) {
				$countA["ItemsOwn"]++;
				$result.=sprintf("%6d  ",$item["EffectiveLevel"]);			
				$values[$id]["EffectiveLevel"]=$item["EffectiveLevel"];
					
				if($findKey=="EL") {
					if($findVal==$item["EffectiveLevel"]) {
						$findRes[]=$id;					
					}				
				}
			} else {
				$result.="     0  ";			
			}

			if(isset($item["EffectiveLevelModifier"])) {
				$result.=sprintf("%6d  ",$item["EffectiveLevelModifier"]);
				$values[$id]["EffectiveLevelModifier"]=$item["EffectiveLevelModifier"];

				if($findKey=="ELM") {
					if($findVal==$item["EffectiveLevelModifier"]) {
						$findRes[]=$id;					
					}				
				}				
			} else {
				$result.="    --  ";			
			}

			if(isset($item["Level"])) {
				$result.=sprintf("%6d  ",$item["Level"]);
				$values[$id]["Level"]=$item["Level"];

				if($findKey=="L") {
					if($findVal==$item["Level"]) {
						$findRes[]=$id;					
					}				
				}				
			} else {
				$result.="     0  ";			
			}
						
			if(isset($item["Name"])) {
				$values[$id]["Name"]=$item["Name"];
				$str=$item["Name"];
				
				if(strlen($str)>10) {
					$str=substr($str,0,strlen($str)-3)."...";					
				}
				$result.=sprintf("%-10s  ",$str);				
			} else {
				$result.="        --  ";			
			}
			
			if(isset($item["Tag"])) {
				$result.=sprintf("%-10s  ",$item["Tag"]);
				$values[$id]["Tag"]=$item["Tag"];
			} else {
				$result.="        --  ";			
			}
			
			$result.=$commonName."\n";
		}
	}

	$result.="===================================================\n";
	$result.="All items: ".sprintf("%3d",$countA["ItemsAll"])."\n";
	$result.=" Existing: ".sprintf("%3d",$countA["ItemsId"])."\n";
	$result.="    Owned: ".sprintf("%3d",$countA["ItemsOwn"])."\n";
	
	$wrA=writeResult($result,$config["INVENT_FILE"],"Inventory saved to ".$config["INVENT_FILE"],1);	
	if(!$wrA[0])
		return array(false,$wrA[1]);		
		
	if($findKey) {
		if($findRes) {
			asort($findRes);
			$result="  Found IDs with value ".$findVal." for property key ".$findKey."\n";
			foreach($findRes as $fr) {
				$idName="";
				if(array_key_exists($fr,$allIDs)) {
					$idName=$allIDs[$fr];
				}
				$result.=$fr.": ".$idName."\n";			
			}
			println("Found ".count($findRes)." IDs with ".$findKey." = ".$findVal,1);
			$wrA=writeResult($result,$config["FIND_FILE"],"Results saved to ".$config["FIND_FILE"],2);
			if(!$wrA[0]) {
				return array(false,$wrA[1]);		
			}			 
		}	else {
			println("NOTE:");
			println("No values of ".$findVal." found for property key ".$findKey,1);		
		}
	}
	
	$sValues=array();
	foreach($ids as $id=>$v) {
		if(isset($values[$id])) {
			$sValues[$id]=$values[$id];
		} else {
			$sValues[$id]=$blankItem;
			if(array_key_exists($id,$allIDs)) {
				$sValues[$id]["CommonName"]=$allIDs[$id];
			}					
		}
	}
	
	if(!$quick) {
		foreach($values as $id=>$v) {
			if(array_key_exists($id,$sValues)) {
				continue;		
			}
			$sValues[$id]=$v;
		}
	}
		
	return array(true,$sValues);
}

// ---------------------------------------------------------

function displayValues($vals,$print=true,$mcm=30,$short=false,$header="") {
	$result="";
	if($header)
		$result="    ".$header."\n";
		
	$frm="%-12s";
	$cm=12;
	
	foreach($vals as $v) {
		$l=strlen($v["CommonName"]);

		if($l>$cm) {
			$cm=$l;		
		}	
	}

	if($cm>$mcm) {
		$frm="%-".$mcm."s";
		$cm=$mcm;
	}	else {
		$frm="%-".$cm."s";	
	}
	
	if($short) {
		$dvals=arrSort($vals,"CommonName");
	} else {
		$dvals=$vals;
	}
	
	$gold=false;
	
	foreach($dvals as $id=>$v) {
		if($id==131137) {
			$gold="    Gold: ".number_format($v["EffectiveLevel"],0,".",",")."\n";
			continue;		
		}
		
		if(strlen($v["CommonName"])>$cm) {
			$result.=sprintf($frm,substr($v["CommonName"],0,$cm-3)."...");
		} else {
			$result.=sprintf($frm,$v["CommonName"].str_repeat(".",$cm-strlen($v["CommonName"])));
		}
		
		if($v["EffectiveLevel"]!==false) {
			$result.=sprintf(" %3d",$v["EffectiveLevel"]);
		} else {
			$result.="     --";		
		}
		
		if(!$short) {
			if($v["Level"]!=$v["EffectiveLevel"]) {
				$result.=sprintf(" / %3d",$v["Level"]);		
			} else {
				$result.="      ";			
			}
						
			if($v["EffectiveLevelModifier"]!==false) {
				$result.=sprintf("  ( %+4d )",$v["EffectiveLevelModifier"]);		
			} else {
				$result.="          ";			
			}
		}
		
		$result.=sprintf("  [ %6d ]",$id);
		$result.="\n";
	}
	
	if($gold)
		$result.=$gold;
		
	if($print)
		echo $result;
}

// ----------------------------------------------------------------

function fillChangeArray($argc,$argv) {
	$retA=array();
	
	for($i=1; $i<$argc; $i++) {
		if($argv[$i]=="-c") {
			if(isset($argv[$i+1]) && isset($argv[$i+2])) {
				$chItem=getItemID($argv[$i+1]);
				if($chItem===false) {
					return array(false,"Unknown item '".$argv[$i+1]."'");				
				}
				if(!is_numeric($argv[$i+2])) {
					return array(false,$argv[$i+2]." is not a numeric value");				
				}
				$retA[$chItem]=intval($argv[$i+2]);
			} else {
				return array(false,"Parameter #".(intval($i/3)+1)." is missing name/value pair");			
			}		
		}	
	}
	
	return array(true, $retA);
}

// ----------------------------------------------------------------

function getItemID($str) {
	global $allIDs;
	
	foreach($allIDs as $id=>$name) {
		if(stripos($name,$str)!==false) {
			return $id;		
		}	
	}
	
	return false;
}

// ----------------------------------------------------------------

function changeValue($id,$newV) {
	global $fileData,$QPLStart,$ver,$dbg;

	$dL=strlen($fileData);
		
	$pos=strrpos($fileData,":".$id);
	if($pos===false)
		return array(false,"Could not find '".$id."' in game save");	
	
	if($pos<$QPLStart)
		return array(false,"Position of '".$id."' (".$pos.") is less than QPL (".$QPLStart.")");

	// item block start is at 2nd { left of ID
	$f=false;
	$pS=$pos;
	$bc=0;
	$dbg.="Finding ID=".$id."\n  ";
		
	while($pS>$QPLStart) {
		$c=substr($fileData,$pS,1);
		if($c=="{") {
			$bc++;
			if($bc==2) {
				$f=true;
				break;			
			}		
		}
		
		$pS--;
	}
	
	if(!$f) {
		$dbg.=substr($fileData,$pos-100,120);
		
		return array(false,"No starting bracket");		
	}
	
	$dbg.="Start: ".$pS."\n  ";
	
	// item block end is at 2nd } right of ID
	$f=false;
	$pE=$pos;
	$bc=0;
	$fdL=strlen($fileData);
	
	while($pE<$fdL) {
		$c=substr($fileData,$pE,1);
		if($c=="}") {
			$bc++;
			if($bc==2) {
				$f=true;
				break;			
			}		
		}
		
		$pE++;
	}
	
	if(!$f) {
		$dbg.=substr($fileData,$pos-100,120);
		
		return array(false,"No ending bracket");		
	}
	
	$dbg.="  End: ".$pE."\n  ";
	$block=substr($fileData,$pS,$pE-$pS+1);
	$dbg.=$block."\n";
	
	$jA=json_decode($block,true);
	if(!$jA)
		return array(false,"Could not decode item block");
		
	$dbg.=print_r($jA,true)."\n";

	$keys=array("EffectiveLevel",
					"EffectiveLevelModifier",
					"Level",
					"AssociatedQuality"
				);
				
	$nbA=array();
				
	$jA["EffectiveLevel"]=$newV;
	$jA["Level"]=$newV;
	
	foreach($keys as $k)
		if(isset($jA[$k]))
			$nbA[$k]=$jA[$k];
		
	$nB=json_encode($nbA);
	$dbg.=$nB;
	$dbg.="\n".str_repeat("=",80)."\n";
	
	$fileData=substr_replace($fileData,$nB,$pS,strlen($block));

	return array(true);
}

// -------------------------------------------------------------------

function insertValue($id,$newV) {
	global $fileData, $dbg;
	
	$eoip=strrpos($fileData,"],\"");
	
	if($eoip===false)
		return array(false,"Couldn't find end-of-items marker");
		
	$nBA=array(	"EffectiveLevel"		=> $newV,
					"Level"					=> $newV,
					"AssociatedQuality"	=> array(
														"Tag"	=> "",
														"Id"	=> $id
													),
			);
			
	$j=json_encode($nBA);
	$dbg.="     Adding ID: ".$id." = ".$newV."\n";
	$dbg.=substr($fileData,$eoip-10,120)."\n";
	$dbg.="  EOA position: ".$eoip."\n";
	$dbg.="  Adding block:\n  ".$j."\n";
	$fileData=substr_replace($fileData,",".$j,$eoip,0);
	
	$dbg.=substr($fileData,$eoip-10,120)."\n====================================\n";

	return array(true);
}

// -----------------------------------------------------------

function worldData($arr,$brief=false) {
	global $config;

	if(!isset($arr["GeneratedWorld"]))
		return array(false,"World data key 'GeneratedWorld' not found");
	
	$result="";
	if($brief)
		return array(true,array($arr["GeneratedWorld"]["CurrentPortId"],
										$arr["GeneratedWorld"]["LocationName"],
										$arr["GeneratedWorld"]["CurrentRegionId"],
										$arr["GeneratedWorld"]["CurrentSegmentPosition"]
									)
					);
	
	$wd=$arr["GeneratedWorld"]["Regions"];
	
	foreach($wd as $k=>$region) {
		$result.="Region: ".$k."\n";
		$result.="  id=".$region["Id"]."\n";
		$fogCount=0;
		foreach($region["FogBools"] as $fg) {
			if($fg) {
				$fogCount++;			
			}		
		}
		$allFogs=count($region["FogBools"]);
		$result.="  ".$fogCount." of ".$allFogs." fogs active (".number_format($fogCount/$allFogs*100,2)."%)\n";
		foreach($region["Segments"] as $sk=>$segment) {
			$result.="  Segment: ".$sk."\n";
			foreach($segment as $sseg) {
				$result.="    ".$sseg["SkylessScene"]["Name"]."\n";
				if(isset($sseg["Discovered"])) {
					$result.="      discovered\n";					
				} else {
					$result.="      hidden\n";
				}
				$discLM=0;
				foreach($sseg["SkylessScene"]["Landmarks"] as $lm) {
					if(isset($lm["IsDiscovered"])) {
						$discLM++;					
					}			
				}
				$allLM=count($sseg["SkylessScene"]["Landmarks"]);
				$result.="      ".$allLM." landmarks, ".$discLM." discovered (".number_format($discLM/$allLM*100,2)."%)\n";				
			}
		}
	}
	
	$wrA=writeResult($result,$config["WORLD_DATA"],"World data saved to ".$config["WORLD_DATA"],1);
	if(!$wrA[0])
		return array(false,$wrA[1]);
		
	return array(true);
}

// -------------------------------------------------------------

function defogRegion($fileData,$regId) {
	global $config;

	$regPos=strpos($fileData,$regId);
	if($regPos===false)
		return array(false,"Region ".$regId." not found");
	
	$fbAPos=strpos($fileData,"FogBools",$regPos);
	if($fbAPos===false)
		return array(false,"'FogBools' key not found");
	
	$dL=strlen($fileData);
	$fbAPos+=11;
	$p=$fbAPos;
	$ebPos=false;
	
	do {
		$c=substr($fileData,$p,1);
		if($c=="]") {
			$ebPos=$p;
			break;		
		}
		if(substr($fileData,$p,8)=="FogTiles") {
			return array(false,"No closing ']' (FT)");		
		}
		$p++;
	} while($p<$dL);

	if($ebPos===false)
		return array(false,"No closing ']' (EOF)");
	
	$defogged=str_repeat("false,",10000);
	$defogged=substr($defogged,0,strlen($defogged)-1);
	$segL=$ebPos-$fbAPos;
	
	$fileData=substr_replace($fileData,$defogged,$fbAPos,$segL);
	
	$wrA=writeResult($fileData,$config["DEFOG_FILE"],"Defogged result saved to ".$config["DEFOG_FILE"],1);
	if(!$wrA[0])
		return array(false,$wrA[1]);
	
	return array(true);
}

// -----------------------------------------------------

function modHull($hbase,$hmod,$ids) {
	global $config;

	$hID=false;
	foreach($ids as $id=>$p) {
		if(strtoupper($p)=="HULL") {
			$hID=$id;
			break;		
		}	
	}
	
	if(!$hID)
		return array(false,"Hull is not specified in ".$config["ALL_IDS"]);
		
	$chDataA=changeHull($hID,$hbase,$hmod);
	if(!$chDataA[0])
		return array(false,$chDataA[1]);
		
	return array(true,true);
}

// ----------------------------------------------------------------

function changeHull($id,$hbase,$hmod) {
	global $fileData,$QPLStart,$dbg;

	$dL=strlen($fileData);
		
	$pos=strrpos($fileData,":".$id);
	if($pos===false)
		return array(false,"Could not find '".$id."' in game save");	
	
	if($pos<$QPLStart)
		return array(false,"Position of '".$id."' (".$pos.") is less than QPL (".$QPLStart.")");
		
	$posL=$pos-1;
	$f=false;
	while($posL>$QPLStart) {
		if(substr($fileData,$posL,5)=="Level") {
			$f=true;
			break;
		}
		
		$posL--;
	}
	
	if(!$f)
		return array(false,"No 'Level' for '".$id."'");
	
	$posL+=7;
		
	$posEL=$pos-1;
	$f=false;
	while($posEL>$QPLStart) {
		if(substr($fileData,$posEL,14)=="EffectiveLevel") {
			$f=true;
			break;
		}
		
		$posEL--;
	}
	
	if(!$f)
		return array(false,"No 'EffectiveLevel' for '".$id."'");
		
	$posEL+=16;
	
	$lvl="";
	for($i=$posL; $i<$dL; $i++) {
		$c=substr($fileData,$i,1);
		if($c==",")
			break;
			
		$lvl.=$c;
	}
	
	if(!is_numeric($lvl))
		return array(false,$lvl." is not a numeric value for '".$id."'");
		
	$e_lvl="";
	for($i=$posEL; $i<$dL; $i++) {
		$c=substr($fileData,$i,1);
		if($c==",")
			break;
			
		$e_lvl.=$c;
	}
	
	if(!is_numeric($e_lvl))
		return array(false,$e_lvl." is not a numeric value for '".$id."'");
	
	$dp=min(($posL-7),($posEL-16));
	$dbg.="Changing: ".$id."\n";	
	$dbg.=" Display: ".$dp."\n";
	$dbg.=substr($fileData,$dp,100)."\n";
	$dbg.="  Pos: ".$pos."-".substr($fileData,$pos,6)."\n";
	$dbg.=" PosL: ".$posL."\n";
	$dbg.="  Lvl: ".$lvl."\n";
	$dbg.="PosEL: ".$posEL."\n";
	$dbg.=" ELvl: ".$e_lvl."\n";
	$dbg.="............................\n";
		
	$fileData=substr_replace($fileData,$hbase,$posL,strlen($lvl));
	$htot=$hbase+$hmod;
	$fileData=substr_replace($fileData,$htot,$posEL,strlen($e_lvl));
	$fileData=substr_replace($fileData,",\"EffectiveLevelModifier\":".$hmod,$posEL+strlen($htot),0);
	
	$dbg.=substr($fileData,$dp,100)."\n";
	$dbg.="============================\n";
	
	return array(true,true);
}

// ------------------------------------------------

function findCandidates($pat) {
	global $allIDs;
	
	$matching=array();
	
	foreach($allIDs as $id=>$name) {
		if(stripos($name,$pat)!==false) {
			$matching[$id]=$name;		
		}	
	}
	
	return $matching;
}

// ------------------------------------------------

function println($str,$indent=0) {
	echo str_repeat(" ",$indent*2).$str."\n";
}

// ------------------------------------------------

function usage() {
	global $config;
	
	println("-b");
	println("create backup in ".$config["BACKUP_DIR"],1);
	println("directory must exist",2);
	
	println("-c <name> <value>");
	println("change value for variable 'name' to 'value'",1);
	println("can be used multiple times",2);
	println("variable name can be partial",2);
	
	println("-cmp");
	println("compare current inventory with previously saved one",1);
	
	println("-df <regionID>");
	println("defog region",1);
	println("use -w to get region IDs",2);
	
	println("-hm <base> <modifier>");
	println("set Hull to 'base' and its modifier to 'modifier'",1);
	println("effectively giving you 'base'+'modifier' Hull",2);
	
	println("-l");
	println("list values for IDs in ".$config["QUICK_IDS"],1);
				
	println("-p <string>");
	println("show value(s) for items with names containing 'string'",1);
	println("note: no wildcards",2);
	println("double-quote 'string' containing spaces",2);
	
	println("-s <valKey> <value>");
	println("search inventory for specific 'value'",1);
	println("of one of 3 possible property keys",1);
	println(" EL = EffectiveLevel",2);
	println("ELM = EffectiveLevelModifier",2);
	println("  L = Level",2);
	
	println("-tp <port name>");
	println("teleport to 'port name'",1);
	println("'port name' can be partial name but unambiguous",2);
	
	println("-v");
	println("show bank content.",1);
		
	println("-w");
	println("display world info",1);
			
}

// -----------------------------------------------------

function innit() {
	$confA=array();
	$cfgFile=getcwd()."/config.txt";
	$validKeys=array(	"SAVE_DIR"		=> false,
							"LINUX_DIR"		=> false,
							"WIN_DIR"		=> false,
							"LINEAGE"		=> false,
							"QUICK_IDS"		=> false,
							"ALL_IDS"		=> false,
							"WORLD_DATA"	=> false,
							"DEFOG_FILE"	=> false,
							"INVENT_FILE"	=> false,
							"FIND_FILE"		=> false,
							"BACKUP_DIR"	=> false,
							"OUTPUT_DIR"	=> false,
							"DEBUG"			=> false
					);
	
	if(!file_exists($cfgFile))
		return array(false,"Config file 'config.txt' does not exist");
		
	$fin=fopen($cfgFile,"r");
	if(!$fin)
		return array(false,"Couldn't open 'config.txt' for reading");
		
	$cfgLs=array();
	$lc=1;
	
	do {
		$l=fgets($fin);
		$l=trim($l);
		if(!$l)
			break;
		
		if(substr($l,0,1)==";") {
			$lc++;
			continue;
		}
		
		$lA=explode("=",$l);
		if(count($lA)!=2) {
			fclose($fin);
			return array(false,"Invalid config format at line ".$lc);		
		}
		
		$cfgLs[]=$lA;
		//echo ".".sprintf("%3d: %-s\n      %s\n",$lc,$lA[0],$lA[1]);		
		$lc++;
	} while($l);
	
	fclose($fin);

	//echo "Read ".$lc." lines\n";
	
	for($i=0; $i<count($cfgLs); $i++) {
		$k=strtoupper($cfgLs[$i][0]);
		if(array_key_exists($k,$validKeys)) {
			$validKeys[$k]=true;
			$confA[$k]=str_replace(PHP_EOL,"",$cfgLs[$i][1]);				
		}	
	}
	
	foreach($validKeys as $key=>$val)
		if(!$val)
			return array(false,"Config key ".$key." missing");
			
	return array(true,$confA);
}

// -----------------------------------------------------------

function writeResult($data,$fileName,$msg=false,$indent=0) {
	global $config;
	
	$od=getcwd()."/".$config["OUTPUT_DIR"];
	if(!file_exists($od)) {
		$wr=mkdir($od);
		if(!$wr) {
			return array(false,"Could not create directory ".$config["OUTPUT_DIR"]);		
		}
	}
	
	if(!is_dir($od)) {
		return array(false,$config["OUTPUT_DIR"]." exists and is not a directory");
	}

	$wr=file_put_contents($od."/".$fileName,$data);
	if($wr===false) {
		return array(false,"Writting of ".$fileName." to ".$config["OUTPUT_DIR"]." failed");			
	}
	
	if($msg) {
		println($msg,$indent);				
	}
	
	return array(true,true);	
}

// -------------------------------------------------------------

function resolveDir() {
	global $config;
	
	if($config["SAVE_DIR"]!="[default]")
		return array(true,$config["SAVE_DIR"]);
	
	$os=php_uname("s");
	$osf=php_uname();
	
	if(strtoupper(substr($os,0,3))=="LIN") {
		$os="LINUX";
	} elseif(strtoupper(substr($os,0,3))=="WIN") {
		$os="WIN";
	} else {
		return array(false,"Unknown OS ".$os);	
	}
	
	$dbg=" Platform:\n  ".$osf."\n";
	
	$uhd=$_SERVER["HOME"];
	$dbg.="Home dir:\n  ".$uhd."\n";
	
	if(file_exists($uhd)) {
		$dbg.="          Exists\n";
	} else {
		return array(false,"Home directory does not exist");	
	}
			
	if(is_dir($uhd)) {
		$dbg.="          Is a directory\n";
	} else {
		return array(false,"Reported save directory is not a directory");	
	}
		
	$sd=$uhd."/".$config[$os."_DIR"]."/Lineage-".$config["LINEAGE"];
	$dbg.="Save dir:\n  ".$sd."\n";
	
	if(file_exists($sd)) {
		$dbg.="          Exists\n";
	} else {
		return array(false,"Save directory does not exist");	
	}
			
	if(is_dir($sd)) {
		$dbg.="          Is a directory\n";
	} else {
		return array(false,"Reported save directory is not a directory");	
	}
	
	if($config["DEBUG"]) {
		$wrA=writeResult($dbg,"platform.dbg");
		if(!$wrA[0]) {
			return array(false,$wrA[1]);		
		}	
	}

	return array(true,$sd);
}

// ----------------------------------------------

function backup() {
	global $config;

	$bckDir=getcwd()."/".$config["BACKUP_DIR"];	
	if(!file_exists($bckDir))
		return array(false,"Backup directory ".$config["BACKUP_DIR"]." does not exist");	
			
	if(!is_dir($bckDir))
		return array(false,$config["BACKUP_DIR"]." is not a directory");	
	
	$existing=scandir($bckDir);
	$free=count($existing)-1;
	for($i=1; $i<$free; $i++) {
		$dfname=$bckDir."/autosave_s.json.".sprintf("%03d",$i);
		if(!file_exists($dfname)) {
			$free=$i;
			break;		
		}	
	}
	$dfname=$bckDir."/autosave_s.json.".sprintf("%03d",$free);
	
	$sfA=resolveDir();
	if(!$sfA[0])
		return array(false,$sfA[1]);
	
	$sfname=$sfA[1]."/autosave_s.json";
	if(!file_exists($sfname)) 
		return array(false,"File\n  ".$sfname."\n  does not exist");
	
	$wr=copy($sfname,$dfname);
	if(!$wr)
		return array(false,"Failed to copy save file to ".$config["BACKUP_DIR"]);
		
	return array(true,basename($dfname));	
}

// ---------------------------------------------------

function readPorts() {
	global $config;
	
	$ffname=getcwd()."/ports.txt";
	if(!file_exists($ffname))
		return array(false,"File does not exist.");
		
	$fin=fopen($ffname,"r");
	if(!$fin)
		return array(false,"Error reading file.");
	
	$retA=array();
	$validKeys=array("CommonName","CurrentRegionId","CurrentPortId","LocationName","CurrentSegmentPosition");
		
	$lc=1;
	do {
		$l=fgets($fin);
		$l=str_replace(PHP_EOL,"",$l);
		$l=trim($l);
		
		if(!$l) break;
		
		if(substr($l,0,1)==";") {
			$lc++;
			continue;		
		}
		
		$segA=explode(",",$l);
		if(count($segA)!=5) {
			fclose($fin);

			if($config["DEBUG"]) {
				$wA=writeResult(print_r($retA,true)."\n".$l."\n".print_r($segA,true),"ports.dbg");
				if(!$wA[0]) {
					return array(false,$wA[1]);		
				}	
			}
			return array(false,"Invalid port definition at line ".$lc);		
		}
	
		$curPort=array();
		
		foreach($segA as $s) {
			$sA=explode(":",$s);
			if(count($sA)<2) {
				fclose($fin);

				if($config["DEBUG"]) {
					$wA=writeResult(print_r($retA,true)."\n".$l."\n".print_r($sA,true),"ports.dbg");
					if(!$wA[0]) {
						return array(false,$wA[1]);		
					}	
				}				
				return array(false,"Invalid port property definition at line ".$lc);
			}
			
			$pk=trim($sA[0],"\"");
			if(array_search($pk,$validKeys)===false) {
				fclose($fin);
				return array(false,"Invalid port property key ".$pk." at line ".$lc);
			}
			
			if($pk=="CurrentSegmentPosition") {
				$pv=trim(str_replace("CurrentSegmentPosition:","",$s));
				if((substr($pv,0,1)!="{") || (substr($pv,strlen($pv)-1,1)!="}")) {
					fclose($fin);
					
					if($config["DEBUG"]) {
						$wA=writeResult(print_r($retA,true)."\n".$l."\n".$s."\n".$pv."\n".print_r($sA,true),"ports.dbg");
						if(!$wA[0]) {
							return array(false,$wA[1]);		
						}	
					}				
					return array(false,"Invalid definition of 'CurrentSegmentPosition' at line ".$lc);				
				}
				$pv=str_replace("/",",",$pv);
			} else {
				if(count($sA)!=2) {
					fclose($fin);
	
					if($config["DEBUG"]) {
						$wA=writeResult(print_r($retA,true)."\n".$l."\n".print_r($sA,true),"ports.dbg");
						if(!$wA[0]) {
							return array(false,$wA[1]);		
						}	
					}				
					return array(false,"Invalid port property definition at line ".$lc);
				} else {
					$pv=trim($sA[1],"\"");
				}
			}
			
			$curPort[$pk]=$pv;
		}
		
		if(array_key_exists($curPort["CurrentPortId"],$retA)) {
			fclose($fin);
	
			if($config["DEBUG"]) {
				$wA=writeResult(print_r($retA,true)."\n".$l."\n".print_r($sA,true),"ports.dbg");
				if(!$wA[0]) {
					return array(false,$wA[1]);		
				}	
			}				
			return array(false,"Duplicate PortId: ".$curPort["CurrentPortId"]." at line ".$lc);		
		}
		$retA[$curPort["CurrentPortId"]]=$curPort;
		$lc++;	
	} while(true);

	fclose($fin);
		
	if($config["DEBUG"]) {
		$wA=writeResult(print_r($retA,true),"ports.dbg");
		if(!$wA[0]) {
			return array(false,$wA[1]);		
		}	
	}
	
	return array(true,$retA);
}

// --------------------------------------------------------------------

function newPort($portA) {
	$fn="newport.dbg";
	$res="Unknown port data\n";
	$res.="Region ID: ".$portA[2]."\n";
	$res.="  Port ID: ".$portA[0]."\n";
	$res.="     Name: ".$portA[1]."\n";
	$res.="  Segment:\n";
	if($portA[3]) {
		foreach($portA[3] as $k=>$v) {
			$res.="      ".$k." = ".$v."\n";		
		}	
	} else {
		   $res.="	   --\n";
	}
	
	$wA=writeResult($res,$fn);
	if(!$wA[0])
		return array(false,$wA[1]);
		
	return array(true);
}

// -------------------------------------------------------------------

function findPort($p,$ports) {
	$f=array();
	
	foreach($ports as $pid=>$pd) 
		if((stripos($pd["CommonName"],$p)!==false) || (stripos($pd["LocationName"],$p)!==false))
			$f[]=$pd;
			
	if(!$f)
		return array(false,"No ports found matching ".$p);
		
	if(count($f)>1)
		return array(false,"Multiple ports (".count($f).") matching ".$p);
		
	return array(true,$f[0]);
}
	
// ----------------------------------------------------------------

function teleport($dport) {
	global $fileData, $config;

	$clP=strpos($fileData,"\"CurrentRegionId\":");
	if($clP===false)
		return array(false,"Can't find location block's start");

	$dbg=substr($fileData,$clP,120);
			
	$clE=strpos($fileData,"},",$clP);
	if($clE===false) {
		if($config["DEBUG"]) {
			$wA=writeResult($dbg,"teleport.dbg");
			if(!$wA[0]) {
				return array(false,$wA[1]);		
			}		
		}
		
		return array(false,"Can't find location block's end");
	}
	
	$clB=substr($fileData,$clP,$clE-$clP);
	$dbg="Current location block:\n  ".$clB."\n";
	$dbg.="Starts at: ".$clP."\n";
	$dbg.="  Ends at: ".$clE."\n";
	$dbg.="Raw:\n".substr($fileData,$clP-8,360)."\n\n";
	
	$nlB="\"CurrentRegionId\":\"".$dport["CurrentRegionId"]."\",";
	$nlB.="\"CurrentPortId\":\"".$dport["CurrentPortId"]."\",";
	$nlB.="\"LocationName\":\"".$dport["LocationName"]."\",";
	$nlB.="\"CurrentSegmentPosition\":".$dport["CurrentSegmentPosition"];
	
	$dbg.="New location block:\n  ".$nlB."\n\n";
	
	$fileData=substr_replace($fileData,$nlB,$clP,strlen($clB));

	$dbg.="Raw:\n".substr($fileData,$clP-8,360);
	
	if($config["DEBUG"]) {
		$wA=writeResult($dbg,"teleport.dbg");
		if(!$wA[0]) {
			return array(false,$wA[1]);		
		}		
	}
	
	return array(true);	
}

// -----------------------------------------------------

function bankContent($arrData) {
	global $allIDs;	
	
	$blankItem=array(	"Name"						=> false,
							"EffectiveLevel"			=> false,
							"EffectiveLevelModifier"=> false,
							"Level"						=> false,
							"Tag"							=> false,
							"ArrayIdx"					=> false,
							"CommonName"				=> chr(124)." unknown"
						);
						
	if(!isset($arrData["SavedBankItems"]))
		return array(false,"Key 'SavedBankItems' not found");

	$vals=array();
			
	foreach($arrData["SavedBankItems"] as $id=>$v) {
		$vals[$id]=$blankItem;
		$vals[$id]["EffectiveLevel"]=$v;
		
		if(array_key_exists($id,$allIDs)) {
			$vals[$id]["CommonName"]=$allIDs[$id];		
		}	
	}
	
	return array(true,$vals);	
}

// ---------------------------------------------------------

function readSavedInv($arrData) {
	global $config, $allIDs;
	
	if(!isset($arrData["QualitiesPossessedList"]))
		return array(false,"'QualitiesPossessedList' key does not exist");
		
	$curInv=$arrData["QualitiesPossessedList"];
	ksort($curInv);	
	
	$sifn=getcwd()."/".$config["OUTPUT_DIR"]."/inventory.txt";
	if(!file_exists($sifn))
		return array(false,"'inventory.txt' does not exist in '".$config["OUTPUT_DIR"]);
		
	$fin=fopen($sifn,"r");
	if(!$fin)
		return array(false,"Could not open 'inventory.txt'");

	$invraw=array();
	$lcnt=1;
			
	do {
		if($lcnt==1) {
			$lcnt++;
			$l=fgets($fin);			
			continue;
		}
		
		$l=fgets($fin);
		if(!$l) break;	
		if(substr($l,0,3)=="===") break;	

		$ldp=strrpos($l,"--");
		if($ldp===false) {
			fclose($fin);
			file_put_contents(getcwd()."/".$config["OUTPUT_DIR"]."/compare.dbg",$l);
			return array(false,"Bad format [--] on line ".$lcnt." in 'inventory.txt'");		
		}		
		
		$lc=substr($l,0,$ldp-1);
		$lc=trim($lc," -\t");
		$idA=explode(":",$lc);
		if(count($idA)!=2) {
			fclose($fin);
			file_put_contents(getcwd()."/".$config["OUTPUT_DIR"]."/compare.dbg",$l);
			return array(false,"Bad format [:] on line ".$lcnt." in 'inventory.txt'");
		}
		
		$id=$idA[0];
		$lc=$idA[1];
		
		$vA=explode(" ",$lc);
		$vals=array();
		$vn=-1;
		
		for($i=0; $i<count($vA); $i++) {
			if(is_numeric($vA[$i])) {
				$vn++;
				$vals[$vn]=intval($vA[$i]);			
			}
			if($vA[$i]=="--") {
				$vn++;
				$vals[$vn]=0;			
			}			
		}		
		
		$invraw[$id]=$vals;		
		$lcnt++;
	} while(true);
	
	fclose($fin);
	
	$dbg=print_r($invraw,true)."\n";
	
	$result="  Inventory comparission\n-----------------------------------------------------------\n";
	$nitems="\n  New items             \n-----------------------------------------------------------\n";
	$dcnt=0;
	$dnew=0;
	
	foreach($curInv as $v) {
		if(isset($v["AssociatedQuality"]["Id"])) {
			$id=$v["AssociatedQuality"]["Id"];
		} else {
			$dbg.="No ID\n";
			$dbg.=print_r($v,true)."\n";
			continue;		
		}
		
		if(array_key_exists($id,$allIDs)) {
			$name=$allIDs[$id];
		} else {
			$name="  unknown";
		}
		
		$diffs="";
		$d=false;
		
		if(!array_key_exists($id,$invraw)) {
			$nitems.=sprintf("%6d:",$id);
			$nitems.=str_pad(sprintf(" %-s ",$name),40,".");
			$dcnt++;
			$dnew++;
			$el=false;
			
			if(isset($v["EffectiveLevel"])) {
				$nitems.=sprintf(" EL=%6d",$v["EffectiveLevel"]);
				$el=true;			
			} else {
				$nitems.=sprintf(" EL=%6d",0);
			}
			
			if(isset($v["EffectiveLevelModifier"])) {
				$nitems.=sprintf("  ELM=%6d",$v["EffectiveLevelModifier"]);			
			} else {
				//$result.=sprintf("  ELM=%6d",0);
				$nitems.=str_repeat(" ",12);
			}
			
			if(isset($v["Level"])) {
				if($el && ($v["Level"]!=$v["EffectiveLevel"])) {
					$nitems.=sprintf("  L=%6d",$v["Level"]);
				}			
			}
			
			$nitems.="\n";					
		} else {	
			$diffs=sprintf("%6d:",$id);
			$diffs.=str_pad(sprintf(" %-s ",$name),40,".");
			
			if(isset($v["EffectiveLevel"])) {
				$d1=$v["EffectiveLevel"]-$invraw[$id][0];			
			} else {
				$d1=0-$invraw[$id][0];
			}
			if($d1) {
				$diffs.=sprintf(" EL=%+6d",$d1);
				$d=true;
			} else {
				$diffs.=str_repeat(" ",10);	
			}
			
			if(isset($v["EffectiveLevelModifier"])) {
				$d2=$v["EffectiveLevelModifier"]-$invraw[$id][1];			
			} else {
				$d2=0-$invraw[$id][1];
			}
			if($d2) {
				$diffs.=sprintf("   ELM=%+6d",$d2);
				$d=true;			
			} else {
				$diffs.=str_repeat(" ",13);
			}
			
			if(isset($v["Level"])) {
				$d3=$v["Level"]-$invraw[$id][2];			
			} else {
				$d3=0-$invraw[$id][2];
			}
			if($d3!=$d1) {
				$d=true;
				$diffs.=sprintf("   L=%+6d",$d3);			
			}
			if($d) {
				$result.=$diffs."\n";
				$dcnt++;
			}
		}		
	}
	
	$result.=$nitems;
	$result.="===========================================================\n";
	$result.=sprintf("    New items: %3d",$dnew)."\n";
	$result.=sprintf("  Differences: %3d",$dcnt)."\n";
	
	file_put_contents(getcwd()."/".$config["OUTPUT_DIR"]."/compare.dbg",$dbg);
	
	$fn="difference.txt";
	$wA=writeResult($result,$fn);
	if(!$wA[0])
		return array(false,$wA[1]);
			
	return array(true);
}

// ---------------------------------------------------------

function arrSort($arr,$col) {
	$retA=array();
	$sa=array();
	
	foreach($arr as $id=>$v)
		$sa[$id]=$v[$col];
		
	asort($sa);
	
	foreach($sa as $id=>$v)
		$retA[$id]=$arr[$id];
		
	return $retA;
}
?>